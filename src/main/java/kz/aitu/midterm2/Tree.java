package kz.aitu.midterm2;

public class Tree {
    private Node root;
    int size = 0;
    int sum = 0;

    public Tree() {
        this.root = null;
    }

    public Node getRoot() {
        return root;
    }

    public void add(int key) {
        set(this.root, key);
    }

    public Node set(Node current, int key) {
        if(current == null) {
            root = new Node(key);
        }
        else {
            if (key >= current.getKey()) {
                current.setRightChild(set(current.getRightChild(), key));
            }
            else {
                current.setLeftChild(set(current.getLeftChild(), key));
            }
        }
        return current;
    }


    public int size() {
        return size;
    }


    public int getHeight(Node root) {
        if (root == null) {
            return 1;
        }
        int leftHeight = 0;
        int rightHeight = 0;

        leftHeight = getHeight(root.getLeftChild());
        rightHeight = getHeight(root.getRightChild());

        if (leftHeight > rightHeight) {
            return rightHeight + 1;
        } else {
            return leftHeight + 1;
        }
    }

    public int TotalSum(Node node) {
    if(node == null){
        return 0;
    }
        TotalSum(node.getLeftChild());
        sum = sum + node.getKey();
        TotalSum(node.getRightChild());
        sum += node.getKey();
        return sum;
    }


    public int findMax( ){
        Node current = root;
        if (current == null){
            return 0;
        }
        while (current.getRightChild() != null){
            current = current.getRightChild();
        }
        return current.getKey();
    }
}
