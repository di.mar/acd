package kz.aitu.midterm2;

import java.util.Scanner;

public class task5 {
    public static void sort(int []arr){
        for (int i= 0; i<arr.length; i++){
            for (int j = 1; j<arr.length-1; j++){
                if (arr[j]> arr[j+1]){
                    int ttmp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = ttmp;
                }
            }
        }
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n  =  input.nextInt();
        int arr[] = new int[n];
        for (int i= 0; i<n; i++){
            arr[i] = input.nextInt();
        }
        int x =0;
        sort(arr);
         int b = input.nextInt();
         for(int i = 0; i<n; i++){
             if (b== arr[i]){
                 x = i;
             }
         }
         System.out.print(x);
    }
}
