package kz.aitu.week9;

public class Queue {
    private Node head;
    private Node tail;
    private int size;

    public Queue(){
        this.head = null;
        this.tail = null;
    }

    public Node getHead() {
        return head;
    }

    public void setHead(Node head) {
        this.head = head;
    }

    public Node getTail() {
        return tail;
    }

    public void setTail(Node tail) {
        this.tail = tail;
    }

    public void add(Node node){
        if (head == null ){
            head = node;
            tail = node;
            size ++;
        }
        else{
            tail.setNext(node);
            node.setNext(null);
            tail = node;
            size++;
        }}
    public Node poll () {
        if (head == null){
            return null;
        }
        Node tmp = head;
        head = head.getNext();
        size--;
        return tmp;
    }
    public int size(){
        return size;
    }
    public int peek(){
        return head.getData();
    }
}
