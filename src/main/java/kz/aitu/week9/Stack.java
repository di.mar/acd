package kz.aitu.week9;

public class Stack {
    private Node top;
    private int size;

    public Stack(){
        this.top = new Node(0);
    }

    public Node getTop() {

        return top;
    }

    public void setTop(Node top)
    {
        top = top;
    }

    public void push(Node node){
        if (top == null){
            top = node;
            size ++;
        }
        else{
            node.setNext(top);
            top = node;
            size++;
        }
    }
    public Node pop(){
        if (top == null){
            return null;
        }
        Node tmp = top;
            top = top.getNext();
            size--;
    return tmp;
    }

    public int size (){
        return size;
    }

    public boolean empty(){
        if (size == 0){
            return true;
        }
        else {
            return false;
        }
    }
    public int top(){
        return top.getData();
    }
}
