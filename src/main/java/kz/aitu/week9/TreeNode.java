package kz.aitu.week9;

public class TreeNode {
    private Integer key;
    private TreeNode left;
    private TreeNode right;

    public TreeNode(Integer key) {
        this.key = key;
        this.left = null;
        this.right = null;
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public TreeNode getLeft() {
        return left;
    }

    public void setLeft(TreeNode left) {
        this.left = left;
    }

    public TreeNode getRight() {
        return right;
    }

    public void setRight(TreeNode right) {
        this.right = right;
    }
}
