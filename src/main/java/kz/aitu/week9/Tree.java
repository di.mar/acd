package kz.aitu.week9;


public class Tree {
    private TreeNode root;

    public Tree() {
        this.root = null;
    }

    public TreeNode getRoot() {
        return root;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public void insert(Integer key) {
        root = set(root, key);
    }

    private TreeNode set(TreeNode current, int key){
        if(current == null) {
            current = new TreeNode(key);
        }
        else {
            if (key >= current.getKey()) {
                current.setRight(set(current.getRight(), key));
            }
            else {
                current.setLeft(set(current.getLeft(), key));
            }
        }
        return current;
    }

    public TreeNode deleteNode(TreeNode root, int value){
        if(root == null) return null;
        if(value < root.getKey()){
            root.setLeft(deleteNode(root.getLeft(), value));
        }else if(value > root.getKey()){
            root.setRight(deleteNode(root.getRight(), value));
        }
        else{
            if(root.getLeft() == null && root.getRight() == null){
                return null;
            }else if(root.getLeft() == null){
                return root.getRight();
            }else if(root.getRight() == null){
                return root.getLeft();
            }
            else {
                Integer minValue = minValue(root.getRight());
                root.setRight(deleteNode(root.getRight(), minValue));
            }
        }
        return root;
    }
    int minValue(TreeNode node) {
        if((node.getLeft() != null)){
            return minValue(node.getLeft());
        }
        return node.getKey();
    }


    public TreeNode findNode(TreeNode node, Integer key) {
        if(node == null) return null;
        if(key > node.getKey()) return findNode(node.getRight(), key);
        else if(key < node.getKey()) return findNode(node.getLeft(), key);
        else return node;
    }


    public void printAllAscending(TreeNode node) {
        if (node == null) {
            return;
        }
        printAllAscending(node.getLeft());
        System.out.println("Key: "+ node.getKey());
        printAllAscending(node.getRight());
    }

    public void printAll() {
        int h = findHeight(root);
        for(int i = 1; i<h+1; i++){
            printThisLevel(root,i);
        }
    }

    private int findHeight(TreeNode node){
        if(node == null){
            return 1;
        }
        else{
            int leftheight = findHeight(node.getLeft());
            int rightheight = findHeight(node.getRight());

            if (leftheight>rightheight){
                return  (rightheight+1);}
            else {
                return  (rightheight+1);
            }
        }
    }

    private void printThisLevel (TreeNode root , int level)
    {
        if (root == null)
            return;
        if (level == 1)
            /* System.out.print(root.getValue() + " ");*/
            System.out.print(root.getKey()+" ");
        else if (level > 1)
        {
            printThisLevel(root.getLeft(), level-1);
            printThisLevel(root.getRight(), level-1);
        }
    }



}
