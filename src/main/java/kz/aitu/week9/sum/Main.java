package kz.aitu.week9.sum;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int arr[] = {1,2,2,4,6,7,7,8,9,10};
        Scanner scan = new Scanner(System.in);
        int sum = scan.nextInt();
        int n = 0;
        int m = 1;
        int left = arr[0];
        int right = arr[arr.length-1];
        while (left+right != sum){
            left = arr[n];
            right = arr[arr.length-m];
            if(left+right > sum){
                m++;
            }
            else if(left+right<sum){
                n++;
            }
        }
            System.out.print(left+" "+right);
        }
    }

