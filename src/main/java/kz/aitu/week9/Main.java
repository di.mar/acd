package kz.aitu.week9;

import kz.aitu.week6.additional.BSTree;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Tree tree = new Tree();
        tree.insert(5);
        tree.insert(4);
        tree.insert(7);
        tree.insert(8);
        tree.insert(2);
        tree.insert(9);
        tree.insert(3);
        tree.insert(6);
        tree.insert(10);
        tree.insert(1);
        tree.printAll();
        System.out.println();
        //System.out.println("aaaaaaaaaaaaa");
        BFS(tree);
        System.out.println();
        //System.out.println("aaaaaaaaaaaaa");
        DFS(tree);
    }

    private static void inputData(int[] array) {
        Random r = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = r.nextInt(10);
        }
    }

    public static void BFS(Tree tree){
        if (tree.getRoot() == null){
            return;
        }
        Queue queue = new Queue();
        queue.add(new Node(tree.getRoot().getKey()));
        for (int i = 0; i<10; i++){
            Node tmp = queue.poll();
            System.out.print(tmp.getData()+" ");
            TreeNode node = tree.findNode(tree.getRoot(), tmp.getData());
            if (node.getLeft() != null){
                queue.add(new Node(node.getLeft().getKey()));
            }
            if(node.getRight() != null){
                queue.add(new Node(node.getRight().getKey()));
            }
        }
    }

    public static void DFS(Tree tree){
        if(tree.getRoot() == null){
            return;
        }
        Stack stack = new Stack();
        stack.push(new Node(tree.getRoot().getKey()));
        for (int i = 0; i<10; i++){
            Node tmp = stack.pop();
            System.out.print(tmp.getData()+" ");
            TreeNode node = tree.findNode(tree.getRoot(), tmp.getData());
            if (node.getLeft() != null){
                stack.push(new Node(node.getLeft().getKey()));
            }
            if(node.getRight() != null){
                stack.push(new Node(node.getRight().getKey()));
        }
    }
    }}

