package kz.aitu.week8quiz;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {
        static int quick (int[] arr, int first, int last)
        {
            int pivot  = arr[last];
            int j = first -1;
            for (int i = 0; i<arr.length-1; i++){
              if (arr[i]< pivot){
                  j++;
                  int temp = arr[i];
                  arr[i] = arr[j];
                  arr[j] = temp;
              }
            }
            j--;
            int temp = arr[j+1];
            arr [j+1] = arr[last];
            arr[last] = temp;

            return  j;
        }

        public  static void sort(int [] arr, int first, int last){
            if (first<last){
                int p = quick(arr, first, last);{
                    quick( arr, first, (p+1));
                    quick( arr, (p-1), last);
                }
            }
        }
        // Complete the quickSort function below.
        static void quickSort(int[] arr) {
            sort(arr, 1 , (arr.length-1));
        }

        public static void main(String[] args) throws IOException {

          int [] arr = {5,8,3,7,9,6};
          quickSort(arr);

          for(int i = 0; i<arr.length; i++){
              System.out.print(arr[i]+" ");
          }
    }}

