package kz.aitu.week1;

import java.util.Scanner;

public class problem701 {
    public int fin (int n){
        if (n==0 || n==1) return n;
        else return  fin(n-1) + fin (n-2);
    }
    public void run()
    {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        System.out.println(fin(n));
    }
}
