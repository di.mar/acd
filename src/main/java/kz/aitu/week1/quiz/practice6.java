package kz.aitu.week1.quiz;

import java.util.Scanner;

public class practice6 {

    public static int recursion(){
        Scanner input = new Scanner(System.in);
      int num1 = input.nextInt();
      if (num1==0) return 0;
      int num2 = recursion();
      if (num2>num1){
          return num2;
      }
        return num1;
    }
    public static void main(String[] args){
        System.out.println(recursion());
    }
}
