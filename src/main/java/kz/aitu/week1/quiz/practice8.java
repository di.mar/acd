package kz.aitu.week1.quiz;
import java.lang.String;
import java.util.Scanner;

public class practice8 {
    public static boolean recursion(String s) {
        char a;
        char b;
        int size = s.length();
        String CutString;
        if (size <= 1) {
            return true;
        } else {
            a = s.toCharArray()[0];
            b = s.toCharArray()[size - 1];
            CutString = s.substring(1, size - 1);
            return a == b && recursion(CutString);
        }
    }
    public static  void main(String[] args){
        String str;
        Scanner input = new Scanner(System.in);
        str = input.next();
        if (recursion(str)) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
    }
}
