package kz.aitu.week1.quiz;

import java.util.Scanner;

public class practice7{
    public static void recursion(int max1, int max2){
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        if (n!=0) {
            if (max1 < n) {
                recursion(n, max1);
            } else if (max2 < n) {
                recursion(n, max2);
            } else {
                recursion(max1, max2);
            }}
            else{
                System.out.println(max2);
            }
        }
    public static void main (String[] args){
        recursion(0,0);
    }
}

