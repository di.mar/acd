package kz.aitu.week1.quiz;
import java.util.Scanner;
public class practice5 {

        public static String recursion(int n) {
            if (n == 1) {
                return "1";
            }
            return recursion(n - 1) + " " + n;
        }
        public static void main(String[] args) {
            Scanner input = new Scanner(System.in);
            int n = input.nextInt();

            System.out.println(recursion(n));
        }
    }

