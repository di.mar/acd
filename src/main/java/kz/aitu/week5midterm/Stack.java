package kz.aitu.week5midterm;

public class Stack {
    private Node top;
    private int size;
    public void Stack(){
        this.top = null;
    }

    public Node getTop() {
        return top;
    }

    public void setTop(Node top) {
        this.top = top;
    }

    public void push(Node node){
        if(top == null){
            top = node;
            size++;
        }
        else {
            node.setNextNode(top);
            top = node;
            size++;
        }}
        public void pop(){
            top = top.getNextNode();
            size--;
        }

    }

