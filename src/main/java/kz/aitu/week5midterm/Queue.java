package kz.aitu.week5midterm;

import kz.aitu.queue.Node;

public class Queue {
    private NodeQueue head;
    private NodeQueue tail;
    private int size;

    public Queue(){
        this.head = null;
        this.tail = null;
    }

    public NodeQueue getHead() {
        return head;
    }

    public void setHead(NodeQueue head) {
        this.head = head;
    }

    public NodeQueue getTail() {
        return tail;
    }

    public void setTail(NodeQueue tail) {
        this.tail = tail;
    }

    public void add(NodeQueue node){
        if (head == null ){
            head = node;
            tail = node;
            size ++;
        }
        else{
            tail.setNext(node);
            node.setNext(null);
            tail = node;
            size++;
        }}
    public void poll () {
        if (head == null){
            return;
        }
        head = head.getNext();
        size--;
    }
    public int size(){
        return size;
    }

  public void oddNum (NodeQueue que){
        if(que ==null){
            return;
        }
        if (que.getLength() %2 != 0){
            System.out.println(que.getData());
        }
        oddNum(que.getNext());
  }

}


