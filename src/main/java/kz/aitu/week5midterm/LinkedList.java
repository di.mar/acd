package kz.aitu.week5midterm;

import kz.aitu.week3.Block;

public class LinkedList {
    private Node head;
    private Node tail;
    private int size;

         public LinkedList(){
            this.head = null;
            this.tail = null;
        }

    public Node getHead() {
        return head;
    }

    public void setHead(Node head) {
        this.head = head;
    }

    public Node getTail() {
        return tail;
    }

    public void setTail(Node tail) {
        this.tail = tail;
    }

    public void push(Node node){
             if(head == null){
                 head = node;
                 tail = node;
                 size++;
             }
             else{
                 tail.setNextNode(node);
                 node.setNextNode(null);
              size++;}
    }
    public int size(){
             return size;
    }
    public void print(Node node) {
        if(node == null) return;
        if(node != null){
            System.out.print(node.getData() + " ");
            node = node.getNextNode();
            print(node);
        }

    }}


