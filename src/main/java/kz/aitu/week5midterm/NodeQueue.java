package kz.aitu.week5midterm;

public class NodeQueue {
    private String data;
    private NodeQueue next;

    public NodeQueue(String data){
        this.data = data;
        this.next = null;
    }

    public String getData() {

        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public NodeQueue getNext() {
        return next;
    }

    public int getLength(){ return data.length();}

    public void setNext(NodeQueue next) {
        this.next = next;
    }
}
