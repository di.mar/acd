package kz.aitu.stack;

public class Node {
    private int Data;
    private Node NextNode;

    public int getData() {
        return Data;
    }

    public void setData(int data) {
        Data = data;
    }

    public Node getNextNode() {
        return NextNode;
    }

    public void setNextNode(Node nextNode) {
        NextNode = nextNode;
    }

    public Node(int data){
        this.Data = data;
    }
}
