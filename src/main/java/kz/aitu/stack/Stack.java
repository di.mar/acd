package kz.aitu.stack;

public class Stack {
    private Node top;
    private int size;

    public Stack(){
        this.top = new Node(0);
    }

    public Node getTop() {

        return top;
    }

    public void setTop(Node top)
    {
        top = top;
    }

    public void push(Node node){
        if (top == null){
            top = node;
            size ++;
        }
        else{
            node.setNextNode(top);
            top = node;
            size++;
        }
    }
    public void pop(){
        if (top == null){
        }
        else{
        top = top.getNextNode();
        size--;
    }}

    public int size (){
        return size;
    }
     public boolean empty(){
        if (size == 0){
            return true;
        }
        else {
            return false;
        }
     }
     public int top(){
        return top.getData();
     }
}
