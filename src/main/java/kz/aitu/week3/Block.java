package kz.aitu.week3;


import lombok.Data;

@Data
public class Block {
    private int value;
    private Block NextBlock;


    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Block getNextBlock() {
        return NextBlock;
    }

    public void setNextBlock(Block nextBlock) {
        NextBlock = nextBlock;
    }
}
