package kz.aitu.practice;

public class Main2 {
    public static void main(String args[]){
        LinkedList linkedList = new LinkedList();
        Node head = linkedList.head();

        linkedList.add( new Node("1"));
        linkedList.add( new Node("2"));
        linkedList.add( new Node("3"));
        linkedList.add( new Node("4"));

        Node middle = null;
        //write your code here - start
        int length = 0;
        Node current = head;
        middle = head;
        while(current.next() != null){
            length++;
            if(length %2 ==0){
                middle = middle.next;
            }
            current = current.next;
        }
        if(length%2 ==1){
            middle = middle.next;
        }

        System.out.println("length of LinkedList: " + length);
        System.out.println("middle element of LinkedList : "+ middle);
    }
}
