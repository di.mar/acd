package kz.aitu.week6.additional;

public class BSTree {
    private Node root;

    public BSTree() {
        this.root = null;
    }

    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }

    public void insert(Integer key, String value) {
      root = set(root, key, value);
    }

    private Node set(Node current, int key, String value){
        if(current == null) {
            current = new Node(key, value);
        }
        else {
            if (key >= current.getKey()) {
                current.setRight(set(current.getRight(), key, value));
            }
            else {
                current.setLeft(set(current.getLeft(), key, value));
            }
        }
        return current;
    }

    public boolean delete(int key){
        Node ToBeDelete = findNode(root,key);
        System.out.print(ToBeDelete.getKey());
        System.out.println(getParent(root, ToBeDelete).getKey());
        if (ToBeDelete.getRight()==null && ToBeDelete.getLeft()== null){
            case1(ToBeDelete);
            return true;
        }
        else if(ToBeDelete.getRight() == null && ToBeDelete.getLeft() != null){
            case2(ToBeDelete);
            return true;
        }
        else if(ToBeDelete.getRight() != null && ToBeDelete.getLeft() == null){
            case3(ToBeDelete);
            return true;
        }
        else if(ToBeDelete.getLeft()!= null && ToBeDelete.getRight() != null){
          case4(ToBeDelete);
            return true;
        }
        else if(ToBeDelete == root && ToBeDelete.getRight()==null && ToBeDelete.getLeft()==null){
            root = null;
        }
        return false;
    }
    public Node getParent (Node parent, Node node){
       /* Node parent = root;*/
        if (parent.getLeft().getKey() == node.getKey()){
            return parent;
        }
        else if (parent.getRight().getKey()==node.getKey()){
            return parent;
        }
        else if(parent.getKey() < node.getKey()){
               parent = getParent( parent.getRight(), node);
            }
        else if(parent.getKey() > node.getKey()){
                parent = getParent(parent.getLeft(), node);
            }
        return parent;
    }
    public void case1(Node node){
        Node parent = getParent(root, node);
        if(parent.getKey()>node.getKey()){
        parent.setLeft(null);
        }
        else {
            parent.setRight(null);
        }
        }

        public void case2(Node node){
            Node parent = getParent(root,node);
            parent.setLeft(node.getLeft());
        }

        public void case3(Node node){
        Node parent = getParent(root, node);
        parent.setRight(node.getRight());
        }

        public void case4( Node node){
          Node root1 = node.getLeft();
            while(root1.getRight() != null){
              root1 = root1.getRight();
          }

            node.setKey(root1.getKey());
            node.setValue(root1.getValue());
            root1.setRight(root1.getLeft());
            System.out.println(root1.getRight().getKey());
        }


    public String find(Integer key) {
        Node node = findNode(root, key);
        if(node == null) return null;
        else return node.getValue();
    }

    private Node findNode(Node node, Integer key) {
        if(node == null) return null;
        if(key > node.getKey()) return findNode(node.getRight(), key);
        else if(key < node.getKey()) return findNode(node.getLeft(), key);
        else return node;
    }

    public String findWithoutRecursion(Integer key) {
        Node current = root;
        while(current!=null) {
            if(key > current.getKey()) current = current.getRight();
            else if(key < current.getKey()) current = current.getLeft();
            else return current.getValue();
        }
        return null;
    }

    public void printAllAscending(Node node) {
        if (node == null) {
            return;
        }
        printAllAscending(node.getLeft());
        System.out.println("Key: "+ node.getKey()+ " value: "+ node.getValue());
        printAllAscending(node.getRight());
    }

    public void printAll() {
         int h = findHeight(root);
         for(int i = 1; i<h+1; i++){
             printThisLevel(root,i);
         }
        }

        private int findHeight(Node node){
          if(node == null){
              return 1;
          }
          else{
              int leftheight = findHeight(node.getLeft());
              int rightheight = findHeight(node.getRight());

          if (leftheight>rightheight){
          return  (rightheight+1);}
          else {
              return  (rightheight+1);
          }
          }
        }

    private void printThisLevel (Node root ,int level)
    {
        if (root == null)
            return;
        if (level == 1)
           /* System.out.print(root.getValue() + " ");*/
        System.out.println("Key: "+ root.getKey()+ " value: "+ root.getValue());
        else if (level > 1)
        {
            printThisLevel(root.getLeft(), level-1);
            printThisLevel(root.getRight(), level-1);
        }
    }

    }





