package kz.aitu.week6.hashtable;

public class Hashtable {
    private Node[] array;
    private int size;

    public Hashtable(int i) {
        array = new Node[i];
        this.size = i;
    }

    public void set(String key, String value){
        int index = key.hashCode() % size;
        Node current = array[index];
        if(current ==null){
            array[index] = new Node(key,value);
        }
        while(current != null){
            if (current.getKey() == key){
                current.setValue(value);
            }
            else if(current.getNext() == null){
                current.setNext(new Node(key,value));
            }
            current = current.getNext();
        }
    }
    public String get(String key){
        int index = key.hashCode()% size;
        Node current = array[index];
            while (current != null){
                if(current.getKey() == key){
                    return current.getValue();
                }
                current = current.getNext();
        }
        return null;
    }
    public void remove (String key){
        int index = key.hashCode() % size;
        Node current = array[index];
        if(current == null){
            return;
        }
        if (current.getKey() == key){
            current = current.getNext();
        }
        while (current.getNext() != null){
            if(current.getNext().getKey() == key){
                current.setNext(current.getNext().getNext());
                return;
            }
            current = current.getNext();
        }
    }

    public int size (){
        int sizeNow = 0;
        for (int i=0; i<array.length; i++){
            Node current = array[i];
            while (current != null){
            sizeNow++;
            current = current.getNext();
        }
         }
        return sizeNow;
    }

    public void print(){
        for (int i=0; i<array.length; i++){
            Node current = array[i];
            while (current!= null){
                System.out.print("Key: "+current.getKey() + " value: "+ current.getValue());
                current = current.getNext();
            }
            System.out.println();
        }
    }

}
