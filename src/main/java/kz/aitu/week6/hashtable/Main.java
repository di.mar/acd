package kz.aitu.week6.hashtable;

public class Main {
    public static void main(String[] args) {
        Hashtable arr1 = new Hashtable(10);
        arr1.set("A", "Abzal");
        arr1.set("B", "Bekzat");
        arr1.set("C", "Cara");
        arr1.set("D", "Dias");
        arr1.set("U", "Ulan");


        System.out.println(arr1.get("A"));

        arr1.remove("U");

        arr1.print();

        System.out.println(arr1.size());
    }
}
