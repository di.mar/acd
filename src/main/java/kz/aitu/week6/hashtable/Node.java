package kz.aitu.week6.hashtable;

public class Node {
        private String key;
        private String value;
        private Node next;

        public Node(String key, String value) {
            this.key = key;
            this.value = value;
            next = null;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public kz.aitu.week6.hashtable.Node getNext() {
            return next;
        }

        public void setNext(kz.aitu.week6.hashtable.Node next) {
            this.next = next;
        }

        public boolean equals(Node o){
            if (this == o) return true;
            if (this.key == o.key){
                return false; }
            return false;
        }  }


