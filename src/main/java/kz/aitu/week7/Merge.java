package kz.aitu.week7;

public class Merge {

    public void mergesort(int[] arr, int left, int mid, int right){
        int size1 = mid-left+1;
        int size2 = right-mid;

        int[] leftSide = new int [size1];
        int[] rightSide = new int [size2];

        for (int i = 0; i < size1; i++)
            leftSide[i] = arr[left + i];
        for (int j = 0; j < size2; j++)
            rightSide[j] = arr[mid + 1+ j];

        int index1 =0, index2=0;
        int n = left;
        while(index1<size1 && index2<size2){
            if(leftSide[index1]<rightSide[index2]){
                arr[n] = leftSide[index1];
                index1++;
            }
            else {
                arr[n] = rightSide[index2];
                index2++;
            }
            n++;
        }
        while (index1<size1){
            arr[n] = leftSide[index1];
            index1++;
            n++;
        }
        while (index2<size2){
            arr[n] = rightSide[index2];
            index2++;
            n++;
        }

    }

    public void merge(int[] arr, int left, int right){
        if (left<right){
            int mid = (left+right)/2;
            merge(arr, left, mid);
            merge(arr, (mid+1),right);
            mergesort(arr, left, mid, right);
        }
    }
    public void sort(int[] arr) {
         merge(arr, 0, arr.length-1);
    }
}
