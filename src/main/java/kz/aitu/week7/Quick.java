package kz.aitu.week7;

public class Quick {
     public int sorting(int[] arr, int first, int last) {
         int p = arr[last];
         int j = first-1;
         for(int i= first; i< last; i++){
         if(arr[i]<p){
             j++;
             int temp = arr[j];
             arr[j] = arr[i];
             arr[i]= temp;
         }}
         int temp = arr[j+1];
         arr[j+1] = arr[last];
         arr[last]= temp;
         return (j+1);
     }

     public void quickSort(int arr[], int first, int last){
         if (first < last){
             int p = sorting(arr,first,last);
             quickSort(arr, first, (p-1));
             quickSort(arr, (p+1), last);
         }
     }

    public void sort( int[] arr){
         quickSort(arr,0, arr.length-1 );
         }}
