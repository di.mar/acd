package kz.aitu;


public class Main {

    public static int [] sort(int[] arr){
        for(int i = 0; i<arr.length-1; i++){
            for (int j = 0; j<arr.length-1-i; j++){
                if(arr[j]> arr[j+1]){
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }
        }
        return arr;
    }

    public static void main(String[] args) {
        int [] arr = {5,8,3,7,9,6};
        sort(arr);
        int i=0, j = 1;
        int size = (arr.length*2)-1;
        int [] arr2 = new int[size];
        int z = 0;
        while (j<arr.length){
            arr2[z] =  arr[i];
            z++;
            arr2[z] = arr[j];
            i++;
            j++;
            z++;
        }
       for(int b = 0; b<size; b++){
           System.out.print(arr2[b]+" ");
        }}
    }
